const { priceCheck, buildCheckoutCartWithDefaultRules } = require("./checkout-cart");

describe('CheckoutCart Test', ()=>{


    it('test priceCheck', () => {

        expect(priceCheck([''])).toBe(0);
        expect(priceCheck(['A'])).toBe(50);
        expect(priceCheck(['A','B'])).toBe(80);
        expect(priceCheck(['C','D','B','A'])).toBe(115);
    
        expect(priceCheck(['A','A'])).toBe(100);
        expect(priceCheck(['A','A','A'])).toBe(130);
        expect(priceCheck(['A','A','A','A'])).toBe(180);
        expect(priceCheck(['A','A','A','A','A'])).toBe(230);
        expect(priceCheck(['A','A','A','A','A','A'])).toBe(260);
    
        expect(priceCheck(['A','A','A','B'])).toBe(160);
        expect(priceCheck(['A','A','A','B','B'])).toBe(175);
        expect(priceCheck(['A','A','A','B','B','D'])).toBe(190);
        expect(priceCheck(['D','A','B','A','B','A'])).toBe(190);
    })

    it('test incremental scanning', () => {
        const checkoutCart = buildCheckoutCartWithDefaultRules();
        
        expect(checkoutCart.getTotal()).toBe(0);
        
        checkoutCart.scan("A");  
        expect(checkoutCart.getTotal()).toBe(50);
        
        checkoutCart.scan("B");  
        expect(checkoutCart.getTotal()).toBe(80);
        
        checkoutCart.scan("A");  
        expect(checkoutCart.getTotal()).toBe(130);
        
        checkoutCart.scan("A");  
        expect(checkoutCart.getTotal()).toBe(160);

        checkoutCart.scan("B");  
        expect(checkoutCart.getTotal()).toBe(175);
    })

})
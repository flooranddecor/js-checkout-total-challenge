/*
    TODO: Determine desired data structure for storing rules.
    * Must store at least what is in the product pricing table in the readme.md 
*/
function CheckoutCart(rules) {

	this.rules = rules;

	/*
		TODO: Create implement function to scan products and return cart total based on rules.
	*/

	this.scan = (productCode) => {
		//TODO: implement me
	}

	this.getTotal = () => {
		//TODO: implement me
		return 0.0;
	}

}

module.exports.CheckoutCart = CheckoutCart;

module.exports.buildCheckoutCartWithDefaultRules = () => {
	/*
		TODO: Create the default price rules data structure based on the table in the readme.md
	*/
	const rules = undefined;

	return new CheckoutCart(rules);
}

/**
 * Returns the total after scanning each productCode in productCodes.
 *
 * @param productCodes list of productCodes to scan.
 * @return the CheckoutCart total
 */
module.exports.priceCheck = (products = []) => {
	const checkoutCart = this.buildCheckoutCartWithDefaultRules();

	products.forEach((product) => checkoutCart.scan(product));

	return checkoutCart.getTotal();
}